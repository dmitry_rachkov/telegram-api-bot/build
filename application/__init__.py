#!/usr/bin/env python
import sys
import traceback
import os
import re
from jinja2 import Template
from tabulate import tabulate
import textwrap
import logging
import telegram
from telegram import ReplyKeyboardMarkup, Update, ReplyKeyboardRemove
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackContext,
)

# telegram-bot section
# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

logger = logging.getLogger(__name__)

ENVIRONMENT, FEATURE = range(2)

reply_keyboard_env = [
    ['Test', 'Production'],
    ['Cancel']
]
markup_env = ReplyKeyboardMarkup(reply_keyboard_env, one_time_keyboard=True)


def start(update: Update, context: CallbackContext) -> int:
    """Start the conversation and ask user for input."""
    update.message.reply_text(
        "Hi! I'm your loyal English Language telegram assistant. "
        "I will help you with data transformation and sending messages! "
        "Please, choose environment now.",
        reply_markup=markup_env,
    )

    return ENVIRONMENT


def enable_environment(update: Update, context: CallbackContext) -> int:
    """Ask the user for info about the selected predefined choice."""
    text = update.message.text
    context.user_data['choice'] = text
    update.message.reply_text(f'You have chosen {text.upper()} environment! '
                              f'Give me something to parse:')
    return FEATURE


def data_handler(user_response, chat_id, update):
    """try/except handler to instantiate transformation"""
    try:
        transformation(user_response, chat_id)
        message = "Sent!"
        update.message.reply_text(
            message,
            reply_markup=ReplyKeyboardRemove(),
        )
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback_in_var = traceback.format_tb(exc_traceback)
        err_mess = str(sys.exc_info()[1])
        error_exception = " ".join([str(x) for x in traceback_in_var])
        if err_mess == 'tuple index out of range':
            err_desc = "Incomplete data provided! " \
                       "Check input data for completeness."
        else:
            err_desc = "Unhandled python error. " \
                       "Please, report this to developer!"
        error = "**ERROR:** " + err_desc + "\n" + err_mess.upper() + "\n" \
                + error_exception
        update.message.reply_text(
            error,
            reply_markup=ReplyKeyboardRemove(),
        )


def received_information(update: Update, context: CallbackContext) -> int:
    """Use user inputand use to create to start data transformation."""
    user_data = context.user_data
    text = update.message.text
    category = user_data['choice']
    environment = user_data['choice'].lower()
    user_data[category] = str(text)
    if environment == 'test':
        chat_id = os.getenv('CHAT_ID_TEST')
        user_response = user_data[category]
        data_handler(user_response, chat_id, update)

        del user_data['choice']
        user_data.clear()
        return ConversationHandler.END
    elif environment == 'production':
        chat_id = os.getenv('CHAT_ID_PROD')
        user_response = user_data[category]
        data_handler(user_response, chat_id, update)

        del user_data['choice']
        user_data.clear()
        return ConversationHandler.END


def done(update: Update, context: CallbackContext) -> int:
    """Display the gathered info and end the conversation."""
    user_data = context.user_data
    if 'choice' in user_data:
        del user_data['choice']

    update.message.reply_text(
        "Bye!",
        reply_markup=ReplyKeyboardRemove(),
    )

    user_data.clear()
    return ConversationHandler.END


def send_photo(markdown, chat_id, token, photo):
    """Send photo via telegram api."""
    bot = telegram.Bot(token=token)
    bot.sendPhoto(parse_mode="MarkdownV2", chat_id=chat_id,
                  photo=open(photo, 'rb'), caption=markdown)


def send_quiz(chat_id, token, question, options, correct_answer, explanation):
    """Send quiz with photo caption via telegram api."""
    bot = telegram.Bot(token=token)
    bot.sendPoll(type='quiz', explanation_parse_mode="MarkdownV2",
                 chat_id=chat_id, question=question, options=options,
                 correct_option_id=correct_answer, explanation=explanation)


def send_message(message, chat_id, token):
    """Send message via telegram api."""
    bot = telegram.Bot(token=token)
    bot.sendMessage(parse_mode="MarkdownV2", chat_id=chat_id, text=message)


def bot_core(api_token) -> None:
    """Run the bot."""
    # Create the Updater and pass it your bot's token.
    updater = Updater(api_token)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # Add conversation handler with the states ENVIRONMENT, FEATURE
    conv_handler = ConversationHandler(
        entry_points=[CommandHandler('start', start)],
        states={
            ENVIRONMENT: [
                MessageHandler(
                    Filters.regex('^(Test|Production)$'), enable_environment
                ),
            ],
            FEATURE: [
                MessageHandler(
                    Filters.text & ~(Filters.command |
                                     Filters.regex('^Cancel')),
                    received_information
                )
            ],
        },
        fallbacks=[MessageHandler(Filters.regex('^Cancel'), done)],
    )

    dispatcher.add_handler(conv_handler)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    updater.idle()


# transformation section
def parser(input_data):
    """Function to parse and structure user-input data."""
    t = tuple(re.split(r'\s\s+', input_data))
    if t[2] == "phrase_of_the_day":
        feature = t[2]
        name = t[3]
        category = t[4]
        inputs = [t[4], t[5], t[6], t[7], t[8], t[9], t[10]]
        wrapped_inputs = ['\n'.join(textwrap.wrap(one_input, width=14))
                          for one_input in inputs]
        parsed_inputs = {'Type': ['Category:', 'Language:', 'Style:',
                                  'Level:', 'Synonyms:',
                                  'Definition:', 'Example:'],
                         'Inputs': wrapped_inputs}
        return parsed_inputs, feature, name, category
    elif t[2] == "quiz_of_the_day":
        feature = t[2]
        name = re.escape(t[3])
        question = t[4]
        options = t[5].split(';')
        correct_answer = t[6]
        explanation = re.escape(t[7])
        category = t[8]
        inputs = [t[8], t[9], t[10], t[11]]
        wrapped_inputs = ['\n'.join(textwrap.wrap(one_input, width=10))
                          for one_input in inputs]
        parsed_inputs = {'Type': ['Category:', 'Language:',
                                  'Style:', 'Level:'],
                         'Inputs': wrapped_inputs}
        return parsed_inputs, feature, name, category, question,\
            options, correct_answer, explanation
    elif t[2] == "most_common_mistakes":
        feature = t[2]
        name = re.escape(t[3])
        transcription = t[4]
        lang_object = t[5]
        example = re.escape(t[6])
        return name, feature, transcription, lang_object, example
    raise Exception('Feature ', t[2],
                    ' is incorrect, insert correct feature name.')


def transformation(input_data, chat_id):
    """Function to define which telegram api action to call and how."""
    token = os.getenv('API_TOKEN')
    feature = parser(input_data)[1]
    if feature == "phrase_of_the_day":
        data, feature, name, category = parser(input_data)
        if category == "Idiom":
            photo = "application/images/idioms2.jpg"
        elif category == "Phrasal verb":
            photo = "application/images/phrasal-verbs.jpg"
        else:
            photo = "application/images/phrase-of-the-day.jpg"
        with open('./application/templates/caption.j2') as template_file:
            caption_template = Template(template_file.read())
        caption = tabulate(data, headers="keys",
                           tablefmt="plain", colalign=("left",))
        markdown = caption_template.render(data=caption,
                                           phrase=name, feature=feature)
        send_photo(markdown, chat_id, token, photo)
    elif feature == "quiz_of_the_day":
        photo = "application/images/English-Quiz.png"
        data, feature, name, category, question, options, \
            correct_answer, explanation = parser(input_data)
        for action in ['send_photo', 'send_quiz', 'send_message']:
            if action == "send_photo":
                with open('./application/templates/caption.j2') \
                        as template_file:
                    caption_template = Template(template_file.read())
                markdown = caption_template.render(phrase=name,
                                                   feature=feature)
                send_photo(markdown, chat_id, token, photo)
            elif action == "send_quiz":
                send_quiz(chat_id, token, question, options,
                          correct_answer, explanation)
            elif action == "send_message":
                with open('./application/templates/message.j2') \
                        as template_file:
                    message_template = Template(template_file.read())
                text = tabulate(data, headers="keys", tablefmt="psql",
                                colalign=("left",))
                message = message_template.render(data=text, feature=feature)
                send_message(message, chat_id, token)
    elif feature == "most_common_mistakes":
        photo = "application/images/pronunciation.jpg"
        name, feature, transcription, lang_object, example = parser(input_data)
        with open('./application/templates/caption.j2') as template_file:
            caption_template = Template(template_file.read())
        markdown = caption_template.render(phrase=name, feature=feature,
                                           transcription=transcription,
                                           lang_object=lang_object,
                                           example=example)
        send_photo(markdown, chat_id, token, photo)
    else:
        raise Exception('Unknown feature type', feature)
