from application import bot_core
import os


if __name__ == "__main__":
    api_token = os.getenv('API_TOKEN')
    bot_core(api_token)
